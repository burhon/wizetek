-- first-1 task
SELECT empid,
    firstname,
    lastname,
    n
FROM HR.Employees,
    (
        SELECT TOP 5 n
        FROM dbo.Nums
    ) as ndbo -- first - 2 task
    -- first-1 task
SELECT empid,
    DATEADD(DAY, n, '2016-06-11') as dt
FROM HR.Employees,
    (
        SELECT TOP 5 n
        FROM dbo.Nums
    ) as ndbo
ORDER BY empid -- Second task
SELECT Customers.custid,
    Customers.companyname,
    Orders.orderid,
    Orders.orderdate
FROM Sales.Customers AS C
    INNER JOIN Sales.Orders AS O ON Customers.custid = Orders.custid;
--  Bu yerda self join qo'lanilgan bu yerda ustunlaga murojat qilish uchun Customers yoki Orders deb emas
--  C va O deb murojat qilis kerak

-- Third task
SELECT O.custid,
    COUNT(*) AS numorders,
(
        SELECT SUM(OD.qty)
        FROM Sales.Orders Odr
            LEFT JOIN Sales.OrderDetails OD ON Odr.orderid = OD.orderid
        WHERE Odr.custid = O.custid
        GROUP BY Odr.custid
    ) AS total
FROM Sales.Orders O
WHERE custid IN (
        SELECT custid
        FROM Sales.Customers
        WHERE country = 'USA'
    )
GROUP BY O.custid

-- Forth task
SELECT C.custid,C.companyname,O.orderid,O.orderdate
FROM Sales.Customers C
LEFT JOIN Sales.Orders O
ON O.custid=C.custid

-- Fivth Task
SELECT C.custid,C.companyname
FROM Sales.Customers C
LEFT JOIN Sales.Orders O
ON O.custid=C.custid
WHERE O.orderid IS NULL

--Sixth task
SELECT C.custid,C.companyname,O.orderid,O.orderdate
FROM Sales.Customers C
INNER JOIN Sales.Orders O
ON O.custid=C.custid
WHERE O.orderdate LIKE '2016-02-12'

--Sventh task
SELECT C.custid,C.companyname,O.orderid,O.orderdate
FROM Sales.Customers C
LEFT JOIN (SELECT * FROM Sales.Orders WHERE orderdate LIKE '2016-02-12') O
ON O.custid=C.custid

--Eighth Task
-- 7 dagi yechimda 2016-02-12 shu vaqtdagi orderlarni qaytaradi bu so'rovda 2016-02-12 vaqt oralig'i yoki Null bo'lsa qaytaradi

--Nineth task
SELECT C.custid,C.companyname,CASE WHEN O.orderid IS NULL THEN 'No' ELSE 'Yes' END AS HasOrderOn20160212
FROM Sales.Customers C
LEFT JOIN (SELECT * FROM Sales.Orders WHERE orderdate LIKE '2016-02-12') O
ON O.custid=C.custid

--Tenth Task
-- UNION da ikkita jadvalni birlashtirgan ikkalasida bor ustuni bitta qilib ketadi 
-- UNION ALL da ikkita jadvalni birlashtirganda ikkalasida borni ikkalasini ham chiqaradi

-- Eleventh
SELECT MName,AName, Roles
FROM Movie
WHERE MName IN (SELECT MName FROM Movie
WHERE Roles='Actor'
GROUP BY MName 
HAVING COUNT(distinct AName)>1)
AND (AName='Tom' OR AName='Bob')

--Tvelfth Task

CREATE FUNCTION TSQLV4.checking_items(@num1 int,@num2 int,@num3 int)
RETURNS int
AS 
BEGIN 
	IF(@num1>@num2)
		BEGIN 
			IF(@num1>@num3)
				BEGIN
					return @num1
				END
			return @num3
		END
		ELSE
		BEGIN
			IF(@num2>@num3)
			BEGIN
				return @num2
			END
		END
		return @num3
END
 
--Select data
SELECT Year1,dbo.checking_items(MAX(Max1),MAX(Max2),MAX(Max3))
FROM TestMax
GROUP BY Year1

-- thirteenth
SELECT prs1.EmpID,prs1.EmpName,prs1.EmpSalary,prs1.MgrID
FROM [dbo].[Person] as prs1
WHERE prs1.EmpSalary>(SELECT prs2.EmpSalary FROM [dbo].[Person] as prs2 WHERE prs2.EmpID=prs1.MgrID )

-- fourteenth


