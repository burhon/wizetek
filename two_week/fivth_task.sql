SELECT empid,
	lastname
FROM HR.Employees
WHERE SUBSTRING(lastname, 1, 1) <> UPPER(SUBSTRING(lastname, 1, 1))
