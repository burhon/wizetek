SELECT C.custid,
	C.companyname
FROM Sales.Customers C
WHERE C.custid IN (
		SELECT custid
		FROM Sales.Orders O
		INNER JOIN Sales.OrderDetails CDetail ON CDetail.orderid = O.orderid
		WHERE CDetail.productid = 12
		)
