SELECT empid,
	firstName,
	lastname
FROM Hr.Employees
WHERE empid NOT IN (
		SELECT DISTINCT empid
		FROM Sales.Orders
		WHERE orderdate >= '2016-05-01'
		)
