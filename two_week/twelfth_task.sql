SELECT custid,
	orderid,
	orderdate,
	empid
FROM (
	SELECT *,
		COUNT(custid) OVER (
			PARTITION BY custid ORDER BY custid
			) AS c
	FROM Sales.Orders
	) AS T
WHERE T.c = (SELECT TOP 1 COUNT(custid)
FROM Sales.Orders
GROUP BY custid
ORDER BY COUNT(custid) DESC)
