SELECT DISTINCT country
FROM Sales.Customers
WHERE country NOT IN (
		SELECT DISTINCT country
		FROM HR.Employees
		)
