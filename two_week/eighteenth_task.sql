SELECT custid,
       mon,
       qty,
       SUM(sum) OVER (PARTITION BY custid ORDER BY mon,custid ROWS BETWEEN UNBOUNDED PRECEDING AND 0 PRECEDING) AS sum
FROM (
         SELECT custid,
                qty,
                MIN(ordermonth) as mon,
                sum(qty) as sum
         FROM Sales.CustOrders
         GROUP BY custid,MONTH(ordermonth),qty
     ) AS T