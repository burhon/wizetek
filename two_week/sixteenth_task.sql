SELECT C.custid,
	C.companyname
FROM Sales.Customers C
WHERE C.custid NOT IN (
		SELECT custid
		FROM Sales.Orders
		WHERE YEAR(orderdate) = 2016
		)
	AND C.custid IN (
		SELECT custid
		FROM Sales.Orders
		WHERE YEAR(orderdate) = 2015
		)
