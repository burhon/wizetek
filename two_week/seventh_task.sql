SELECT TOP 3 shipcountry,
	AVG(freight) AS avgfreight
FROM Sales.Orders
WHERE datepart(YEAR, shippeddate) = '2015'
GROUP BY shipcountry
ORDER BY avgfreight DESC
