SELECT custid,
		region
	FROM Sales.Customers
	WHERE Coalesce(region, 'unknown') <> 'unknown'

UNION

SELECT custid,
	region
FROM Sales.Customers
WHERE Coalesce(region, 'unknown') = 'unknown'
ORDER BY region

-- FALSE