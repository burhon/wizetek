USE intrenship
GO

-- Create Table Employee(
--     id int PRIMARY KEY IDENTITY(1,1),
--     salary int 
-- )   
-- Insert into Employee(salary) VALUES(100)
-- Insert into Employee(salary) VALUES(200)
-- Insert into Employee(salary) VALUES(300)
CREATE TABLE Department (
	id INT PRIMARY KEY IDENTITY(1, 1),
	name NVARCHAR(20) NOT NULL
	)

CREATE TABLE Employee (
	id INT PRIMARY KEY IDENTITY(1, 1),
	name NVARCHAR(20) NOT NULL,
	salary INT NOT NULL,
	departmentId INT FOREIGN KEY REFERENCES Department(id)
	)

INSERT INTO Department (
	name
	)
VALUES ('IT')

INSERT INTO Department (
	name
	)
VALUES ('Sales')

INSERT INTO Employee (
	name,
	salary,
	departmentId
	)
VALUES ('Joe',70000,1)

INSERT INTO Employee (
	name,
	salary,
	departmentId
	)
VALUES ('Jim',90000,1)


INSERT INTO Employee (
	name,
	salary,
	departmentId
	)
VALUES ('Henry',80000,2)
-- DELETE FROM Employee WHERE name='Henry';


INSERT INTO Employee (
	name,
	salary,
	departmentId
	)
VALUES ('Sam',60000,2)


INSERT INTO Employee (
	name,
	salary,
	departmentId
	)
VALUES ('Max',90000,1)


/* Write your T-SQL query statement below */
WITH EmployeeMaxSalary(id,Salary) 
as (
)

SELECT * FROM EmployeeMaxSalary


SELECT DISTINCT D.name,
    E.name,
	E.Salary
FROM Employee E
LEFT JOIN Department D ON E.departmentId = D.id
INNER JOIN (
    SELECT id, MAX(salary) OVER (
				PARTITION BY departmentId ORDER BY departmentId
        ) Salary
		FROM Employee) EM
ON EM.salary=E.salary
WHERE E.id in (Select id FROM (
    
Select id,Salary
From Employee
UNION

    SELECT id, MAX(salary) OVER (
				PARTITION BY departmentId ORDER BY departmentId
        ) Salary
		FROM Employee
 ) as tblEmployee
GROUP BY id
HAVING COUNT(*)=1 )
ORDER BY E.name
Select * From Employee

       
		