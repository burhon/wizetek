-- One task
SELECT ProductID,
	COUNT(*)
FROM dbo.ProductCostHistory
GROUP BY ProductID
ORDER BY ProductID

-- Two Task
SELECT CustomerID,
	COUNT(*) AS TotalOrders
FROM dbo.SalesOrderHeader
GROUP BY CustomerID
ORDER BY TotalOrders DESC

-- three Task
SELECT ot.ProductID,
	MIN(oh.OrderDate),
	MAX(oh.OrderDate)
FROM dbo.SalesOrderHeader oh
INNER JOIN dbo.SalesOrderDetail ot ON oh.SalesOrderID = ot.SalesOrderID
GROUP BY ot.ProductID
ORDER BY ProductID

-- Four Task
SELECT ot.ProductID,
	(
		SELECT ProductName
		FROM dbo.Product
		WHERE ProductID = ot.ProductID
		) AS ProductName,
	MIN(oh.OrderDate),
	MAX(oh.OrderDate)
FROM dbo.SalesOrderHeader oh
INNER JOIN dbo.SalesOrderDetail ot ON oh.SalesOrderID = ot.SalesOrderID
GROUP BY ot.ProductID
ORDER BY ProductID

-- Five task
SELECT ProductID,
	StandardCost
FROM ProductCostHistory
WHERE (
		StartDate <= '2012-04-15'
		AND EndDate IS NULL
		)
	OR (
		StartDate <= '2012-04-15'
		AND EndDate >= '2012-04-15'
		)
ORDER BY ProductID

-- six Task
SELECT ProductID,
	StandardCost
FROM ProductCostHistory
WHERE (
		StartDate <= '2014-04-15'
		AND EndDate IS NULL
		)
	OR (
		StartDate <= '2014-04-15'
		AND EndDate >= '2014-04-15'
		)
ORDER BY ProductID

-- Seven task
SELECT SUBSTRING(convert(VARCHAR, EOMONTH(StartDate), 111), 0, 8),
	COUNT(1) [StartDate Count]
FROM ProductListPriceHistory
GROUP BY EOMONTH(StartDate);

-- Eight Task
DECLARE @start VARCHAR(50) = (
		SELECT MIN(FORMAT(StartDate, 'yyyy/MM - MMM'))
		FROM ProductListPriceHistory
		)
DECLARE @end VARCHAR(50) = (
		SELECT MAX(FORMAT(EndDate, 'yyyy/MM - MMM'))
		FROM ProductListPriceHistory
		)

SELECT C.CalendarMonth,
	COUNT(StartDate) AS TotalRows
FROM Calendar C
LEFT JOIN ProductListPriceHistory PH ON C.CalendarDate = PH.StartDate
WHERE C.CalendarMonth BETWEEN @start
		AND @end
GROUP BY C.CalendarMonth
ORDER BY C.CalendarMonth

-- Nine Task
SELECT ProductID,
	MAX(ListPrice)
FROM dbo.ProductListPriceHistory
GROUP BY ProductID

-- Ten Task
SELECT P.ProductID,
	P.ProductName
FROM dbo.Product P
RIGHT JOIN dbo.Product Pt ON Pt.ProductID = P.ProductID
ORDER BY P.ProductID

-- Eleven Task
SELECT ProductID
FROM dbo.ProductCostHistory
WHERE ProductID NOT IN (
		SELECT ProductID
		FROM ProductCostHistory
		WHERE '2014-04-15' BETWEEN StartDate
				AND IsNull(EndDate, getdate())
		)
ORDER BY ProductID

-- Twelve Task
SELECT ProductID
FROM dbo.ProductListPriceHistory
WHERE EndDate IS NULL
GROUP BY ProductID
HAVING COUNT(*) > 1

-- Thirteen task
SELECT P.ProductID,
	P.ProductName,
	PS.ProductSUbcategoryName,
	od.FirstOrder,
	od.LastOrder
FROM dbo.Product P
LEFT JOIN (
	SELECT ot.ProductID ProductID,
		MIN(oh.OrderDate) AS FirstOrder,
		MAX(oh.OrderDate) AS LastOrder
	FROM dbo.SalesOrderDetail ot
	INNER JOIN dbo.SalesOrderHeader oh ON oh.SalesOrderID = ot.SalesOrderID
	GROUP BY ot.ProductID
	) AS Od ON od.ProductID = P.ProductID
LEFT JOIN dbo.ProductSubcategory PS ON PS.ProductSUbcategoryID = P.ProductSUbcategoryID
-- FourTeen task
WITH ProductMaxDate AS (
		SELECT ProductID,
			ListPrice
		FROM dbo.ProductListPriceHistory
		WHERE EndDate IS NULL
		)

SELECT P.ProductID,
	P.ProductName,
	P.ListPrice AS Prod_ListPrice,
	PL.ListPrice AS PriceHist_LatestListPrice,
	P.ListPrice - PL.ListPrice AS Diff
FROM ProductMaxDate PL
INNER JOIN dbo.Product P ON P.ProductId = PL.ProductID
WHERE P.ListPrice - PL.ListPrice <> 0

-- Fifteen Task
SELECT dt.ProductID,
	p.ProductName,
	dh.OrderDate,
	dt.OrderQty Qty,
	p.SellStartDate,
	p.SellEndDate
FROM dbo.SalesOrderDetail dt
INNER JOIN dbo.SalesOrderHeader dh ON dt.SalesOrderID = dh.SalesOrderID
INNER JOIN dbo.Product p ON p.ProductID = dt.ProductID
WHERE dh.OrderDate > p.SellEndDate
	OR dh.OrderDate < p.SellStartDate
ORDER BY p.ProductID

-- Sixteen task
SELECT dt.ProductID,
	dh.OrderDate,
	dt.OrderQty Qty,
	p.SellStartDate,
	p.SellEndDate,
	(
		CASE 
			WHEN dh.OrderDate > p.SellEndDate
				THEN 'Sold after end date'
			ELSE 'Sold before start date'
			END
		) AS ProblemType
FROM dbo.SalesOrderDetail dt
INNER JOIN dbo.SalesOrderHeader dh ON dt.SalesOrderID = dh.SalesOrderID
INNER JOIN dbo.Product p ON p.ProductID = dt.ProductID
WHERE dh.OrderDate > p.SellEndDate
	OR dh.OrderDate < p.SellStartDate
ORDER BY p.ProductID,
	dh.OrderDate

-- Seventeen Task
-- Eighteen Task
SELECT Product.ProductID,
	ProductName,
	ProductSubCategoryName,
	FirstOrder = Convert(DATE, Min(OrderDate)),
	LastOrder = Convert(DATE, Max(OrderDate))
FROM Product
LEFT JOIN SalesOrderDetail Detail ON Product.ProductID = Detail.ProductID
LEFT JOIN SalesOrderHeader Header ON Header.SalesOrderID = Detail.SalesOrderID
LEFT JOIN ProductSubCategory ON ProductSubCategory.ProductSubCategoryID = Product.ProductSubCategoryID
WHERE Color = 'Silver'
GROUP BY Product.ProductID,
	ProductName,
	ProductSubCategoryName
ORDER BY LastOrder DESC

-- NineTeen Task
-- Twenty Task
SELECT S.CustomerID,
	(
		SELECT FirstName + ' ' + LastName
		FROM dbo.Customer
		WHERE CustomerID = S.CustomerID
		) CustomerName,
	COUNT(*) TotalDifferentSalesPeople
FROM dbo.SalesOrderHeader S
GROUP BY S.CustomerID
HAVING COUNT(SalesPersonEmployeeID) > 1

-- Twenty one Task
-- Twenty two Task
SELECT ProductName
FROM dbo.Product
GROUP BY ProductName
HAVING COUNT(*) = 2

-- Twenty three Task
SELECT (
		SELECT TOP 1 ProductID
		FROM dbo.Product
		WHERE ProductName = P.ProductName
		ORDER BY ProductID DESC
		) PotentialDuplicateProductID,
	P.ProductName
FROM dbo.Product P
GROUP BY P.ProductName
HAVING COUNT(*) = 2

-- Twenty four Task
SELECT RowItemCount TotalPriceChanges,
	(
		SELECT COUNT(*)
		FROM (
			SELECT ProductID,
				COUNT(*) RowItemCount
			FROM dbo.ProductCostHistory
			GROUP BY ProductID
			HAVING COUNT(3) = RowItemCount
			) newTb
		) TotalProducts
FROM (
	SELECT ProductID,
		COUNT(*) RowItemCount
	FROM dbo.ProductCostHistory
	GROUP BY ProductID
	) AS NeTab
GROUP BY RowItemCount

-- Twenty Five Task
SELECT ProductName,
	ProductNumber,
	CHARINDEX('-', ProductNumber) AS HyphenLocation,
	IIF(CHARINDEX('-', ProductNumber) = 0, ProductNumber, SUBSTRING(ProductNumber, 0, CHARINDEX('-', ProductNumber))) AS BaseProductNumber,
	IIF(CHARINDEX('-', ProductNumber) = 0, NULL, SUBSTRING(ProductNumber, CHARINDEX('-', ProductNumber) + 1, LEN(ProductNumber))) AS Size
FROM Product
WHERE ProductID > 533
-- Twenty Six Task
WITH tmp AS (
		SELECT ProductName,
			ProductNumber,
			ProductSubcategoryID,
			IIF(CHARINDEX('-', ProductNumber) = 0, ProductNumber, SUBSTRING(ProductNumber, 0, CHARINDEX('-', ProductNumber))) AS BaseProductNumber,
			IIF(CHARINDEX('-', ProductNumber) = 0, NULL, SUBSTRING(ProductNumber, CHARINDEX('-', ProductNumber) + 1, LEN(ProductNumber))) AS Size
		FROM Product
		WHERE ProductID > 533
		)

SELECT BaseProductNumber,
	COUNT(*) AS TotalSizes
FROM tmp
INNER JOIN ProductSubcategory PC ON tmp.ProductSubcategoryID = PC.ProductSubcategoryID
WHERE PC.ProductCategoryID = 3
GROUP BY BaseProductNumber;

--Twenty Seven Task
WITH tmp
AS (
	SELECT ProductID,
		StandardCost,
		LAG(StandardCost) OVER (
			PARTITION BY ProductID ORDER BY StartDate
			) AS ncost
	FROM ProductCostHistory
	)
SELECT tmp.ProductID,
	P.ProductName,
	count(tmp.StandardCost) AS TotalCostChanges
FROM tmp
INNER JOIN Product P ON P.ProductID = tmp.ProductID
WHERE tmp.StandardCost <> ncost
	OR ncost IS NULL
GROUP BY tmp.ProductID,
	P.ProductName;

--Twenty eight Task
WITH tmp
AS (
	SELECT ProductID,
		StandardCost,
		StartDate,
		LAG(StandardCost) OVER (
			PARTITION BY ProductID ORDER BY ProductID
			) AS ncost
	FROM ProductCostHistory
	)
SELECT tmp.ProductID,
	tmp.StartDate,
	P.ProductName,
	tmp.ncost - tmp.StandardCost AS PriceDifference
FROM tmp
INNER JOIN Product P ON P.ProductID = tmp.ProductID
WHERE tmp.StandardCost <> ncost
ORDER BY PriceDifference DESC
--Twenty nine task
WITH FraudSuspects AS (
		SELECT *
		FROM Customer
		WHERE CustomerID IN (29401, 11194, 16490, 22698, 26583, 12166, 16036, 25110, 18172, 11997, 26731)
		),
	SampleCustomers AS (
		SELECT TOP 100 *
		FROM Customer
		WHERE CustomerID NOT IN (29401, 11194, 16490, 22698, 26583, 12166, 16036, 25110, 18172, 11997, 26731)
		ORDER BY NewID()
		)

SELECT *
FROM FraudSuspects

UNION

SELECT *
FROM SampleCustomers
	-- Thirty Task
	WITH tmp AS (
		SELECT ProductID,
			CalendarDate
		FROM ProductListPriceHistory,
			Calendar
		WHERE CalendarDate BETWEEN StartDate
				AND EndDate
		)

SELECT CalendarDate,
	ProductID,
	Count(*) AS TotalRows
FROM tmp
GROUP BY CalendarDate,
	ProductID
HAVING Count(*) > 1

--Thirty-one Task
SELECT C.CalendarDate,
	PL.ProductID,
	COUNT(*) TotalRows
FROM ProductListPriceHistory PL
JOIN Calendar C ON PL.StartDate <= C.CalendarDate
	AND IIF(PL.EndDate IS NULL, GETDATE(), PL.EndDate) >= C.CalendarDate
GROUP BY PL.ProductID,
	C.CalendarDate
HAVING COUNT(*) >= 2;

--Thirty-two Task
DECLARE @dn DATE;

SET @dn = '2014-06-30';

SELECT CalendarMonth,
	COUNT(DISTINCT SalesOrderID) AS TotalOrders,
	SUM(COUNT(DISTINCT SalesOrderID)) OVER (
		ORDER BY CalendarMonth
		) AS RunningTotal
FROM SalesOrderHeader
INNER JOIN Calendar ON CalendarMonth = FORMAT(OrderDate, 'yyyy/MM - MMM')
WHERE OrderDate BETWEEN DATEADD(YEAR, - 1, @dn)
		AND @dn
GROUP BY CalendarMonth
ORDER BY CalendarMonth
-- Thirty-three Task
WITH tmp AS (
		SELECT ST.TerritoryID,
			ST.TerritoryName,
			ST.CountryCode,
			DueDate,
			ShipDate
		FROM SalesOrderHeader SOH
		INNER JOIN SalesTerritory ST ON SOH.TerritoryID = ST.TerritoryID
		)

SELECT TerritoryID,
	TerritoryName,
	CountryCode,
	COUNT(*) AS TotalOrders,
	COUNT(IIF(ShipDate > DueDate, 0, NULL)) AS TotalLateOrders
FROM tmp
GROUP BY TerritoryID,
	TerritoryName,
	CountryCode
ORDER BY TerritoryID;

-- Thirty-four Task
-- Thirty-six Task
WITH tmp
AS (
	SELECT OT.SalesOrderID,
		TE.EventName,
		OT.EventDateTime AS TrackingEventDate,
		LEAD(EventDateTime) OVER (
			PARTITION BY SalesOrderID ORDER BY EventDateTime
			) AS NextTrackingEventDate
	FROM OrderTracking OT
	INNER JOIN TrackingEvent TE ON OT.TrackingEventID = TE.TrackingEventID
	WHERE SalesOrderID IN (68857, 70531, 70421)
	)
SELECT tmp.*,
	DATEDIFF(hour, TrackingEventDate, NextTrackingEventDate) HoursInStage
FROM tmp;

-- Thirty-seven Task
WITH tmp
AS (
	SELECT OT.SalesOrderID,
		TE.EventName,
		OT.TrackingEventID,
		OT.EventDateTime AS TrackingEventDate,
		LEAD(EventDateTime) OVER (
			PARTITION BY SalesOrderID ORDER BY EventDateTime
			) AS NextTrackingEventDate
	FROM OrderTracking OT
	INNER JOIN TrackingEvent TE ON OT.TrackingEventID = TE.TrackingEventID
	)
SELECT CASE 
		WHEN OnlineOrderFlag = 1
			THEN 'Online'
		ELSE 'Offline'
		END AS OnlineOfflineStatus,
	tmp.EventName,
	AVG(DATEDIFF(hour, TrackingEventDate, NextTrackingEventDate))
FROM tmp
INNER JOIN SalesOrderHeader SOH ON SOH.SalesOrderID = tmp.SalesOrderID
GROUP BY tmp.EventName,
	CASE 
		WHEN OnlineOrderFlag = 1
			THEN 'Online'
		ELSE 'Offline'
		END,
	TrackingEventID
ORDER BY OnlineOfflineStatus,
	TrackingEventID
-- Thirty-eight Task
WITH tmp AS (
		SELECT OT.SalesOrderID,
			TE.EventName,
			OT.TrackingEventID,
			OT.EventDateTime AS TrackingEventDate,
			LEAD(EventDateTime) OVER (
				PARTITION BY SalesOrderID ORDER BY EventDateTime
				) AS NextTrackingEventDate
		FROM OrderTracking OT
		INNER JOIN TrackingEvent TE ON OT.TrackingEventID = TE.TrackingEventID
		)

SELECT tmp.EventName,
	AVG(CASE 
			WHEN SOH.OnlineOrderFlag = 0
				THEN DATEDIFF(hour, TrackingEventDate, NextTrackingEventDate)
			ELSE NULL
			END) OfflineAvgHoursInStage,
	AVG(CASE 
			WHEN SOH.OnlineOrderFlag = 1
				THEN DATEDIFF(hour, TrackingEventDate, NextTrackingEventDate)
			ELSE NULL
			END) OnlineAvgHoursInStage
FROM tmp
INNER JOIN SalesOrderHeader SOH ON SOH.SalesOrderID = tmp.SalesOrderID
GROUP BY tmp.EventName,
	TrackingEventID
ORDER BY tmp.TrackingEventID;

-- Fourty Task
WITH Gaps (
	ProductID,
	StartDate,
	endDate2
	)
AS (
	SELECT ProductID,
		StartDate,
		LAG(EndDate) OVER (
			PARTITION BY ProductID ORDER BY ProductID
			) endDate2
	FROM ProductListPriceHistory
	)
SELECT G.ProductID,
	C.CalendarDate
FROM Gaps G
JOIN Calendar C ON C.CalendarDate > G.endDate2
	AND C.CalendarDate < G.StartDate;
