-- Task 1
SELECT ProductID,
	COUNT(*) AS TotalPriceChanges
FROM dbo.ProductCostHistory
GROUP BY ProductID
ORDER BY ProductID

-- Task 2
SELECT CustomerID,
	COUNT(*) AS TotalOrders
FROM dbo.SalesOrderHeader
GROUP BY CustomerID
ORDER BY TotalOrders DESC

-- Task 3 
SELECT DISTINCT D.ProductID,
	FIRST_VALUE(H.OrderDate) OVER (
		PARTITION BY D.ProductID ORDER BY D.ProductID
		) AS FirstOrder,
	LAST_VALUE(H.OrderDate) OVER (
		PARTITION BY D.ProductID ORDER BY D.ProductID
		) AS LastOrder
FROM dbo.SalesOrderDetail D
INNER JOIN dbo.SalesOrderheader H ON H.SalesOrderID = D.SalesOrderID
ORDER BY D.ProductID

-- Task 4 
SELECT DISTINCT D.ProductID,
	P.ProductName,
	FIRST_VALUE(H.OrderDate) OVER (
		PARTITION BY D.ProductID ORDER BY D.ProductID
		) AS FirstOrder,
	LAST_VALUE(H.OrderDate) OVER (
		PARTITION BY D.ProductID ORDER BY D.ProductID
		) AS LastOrder
FROM dbo.SalesOrderDetail D
INNER JOIN dbo.SalesOrderheader H ON H.SalesOrderID = D.SalesOrderID
INNER JOIN dbo.Product P ON P.ProductID = D.ProductID
ORDER BY D.ProductID

-- Task 5
SELECT ProductID,
	StandardCost
FROM dbo.ProductCostHistory
WHERE StartDate <= '2012-04-15'
	AND EndDate >= '2012-04-15'
	OR StartDate <= '2012-04-15'
	AND EndDate IS NULL

-- Task 6
SELECT ProductID,
	StandardCost
FROM dbo.ProductCostHistory
WHERE (
		StartDate <= '2014-04-15'
		AND EndDate >= '2014-04-15'
		)
	OR (
		StartDate <= '2014-04-15'
		AND EndDate IS NULL
		)

-- Task 7
SELECT CAST(DATEPART(YEAR, StartDate) AS VARCHAR) + '/' + CASE 
		WHEN DATEPART(MONTH, StartDate) % 10 = 0
			THEN CAST(DATEPART(MONTH, StartDate) AS VARCHAR)
		ELSE '0' + CAST(DATEPART(MONTH, StartDate) AS VARCHAR)
		END AS CalendarMonth,
	COUNT(*) AS TotalRows
FROM dbo.ProductListPriceHistory
GROUP BY DATEPART(YEAR, StartDate),
	DATEPART(MONTH, StartDate)

-- Task 8
DECLARE @start VARCHAR(50) = (
		SELECT MIN(FORMAT(StartDate, 'yyyy/MM - MMM'))
		FROM ProductListPriceHistory
		)
DECLARE @end VARCHAR(50) = (
		SELECT MAX(FORMAT(EndDate, 'yyyy/MM - MMM'))
		FROM ProductListPriceHistory
		)

SELECT C.CalendarMonth,
	CASE 
		WHEN MAX(TotalRows) IS NULL
			THEN 0
		ELSE MAX(TotalRows)
		END AS TotalRows
FROM dbo.Calendar C
LEFT JOIN (
	SELECT CAST(DATEPART(YEAR, StartDate) AS VARCHAR) + '/' + CASE 
			WHEN DATEPART(MONTH, StartDate) % 10 = 0
				THEN CAST(DATEPART(MONTH, StartDate) AS VARCHAR)
			ELSE '0' + CAST(DATEPART(MONTH, StartDate) AS VARCHAR)
			END + ' - ' + DATENAME(month, DateAdd(month, DATEPART(MONTH, StartDate), - 1)) AS CalendarMonth,
		COUNT(*) AS TotalRows
	FROM dbo.ProductListPriceHistory
	GROUP BY DATEPART(YEAR, StartDate),
		DATEPART(MONTH, StartDate)
	) AS P ON P.CalendarMonth = C.CalendarMonth
WHERE C.CalendarMonth BETWEEN @start
		AND @end
GROUP BY C.CalendarMonth
ORDER BY C.CalendarMonth

-- Task 9
SELECT DISTINCT ProductID,
	LAST_VALUE(ListPrice) OVER (
		PARTITION BY ProductID ORDER BY ProductID
		)
FROM dbo.ProductListPriceHistory

SELECT ProductID,
	ListPrice
FROM [ProductListPriceHistory]
WHERE EndDate IS NULL
ORDER BY ProductID;

-- Task 10
SELECT P.ProductID,
	P.ProductName
FROM dbo.Product P
WHERE P.ProductID NOT IN (
		SELECT ProductID
		FROM dbo.ProductListPriceHistory
		)

-- Task 11
SELECT ProductID
FROM dbo.ProductCostHistory
WHERE ProductID NOT IN (
		SELECT ProductID
		FROM ProductCostHistory
		WHERE '2014-04-15' BETWEEN StartDate
				AND IsNull(EndDate, getdate())
		)
ORDER BY ProductID

-- Task 12
SELECT ProductID
FROM dbo.ProductListPriceHistory
WHERE EndDate IS NULL
GROUP BY ProductID
HAVING COUNT(*) > 1

-- Task 13
SELECT P.ProductID,
	P.ProductName,
	PC.ProductSubcategoryName,
	PO.FirstOrder,
	PO.LastOrder
FROM dbo.Product P
LEFT JOIN (
	SELECT SD.ProductID,
		MIN(SH.OrderDate) AS FirstOrder,
		MAX(SH.OrderDate) AS LastOrder
	FROM dbo.SalesOrderDetail SD
	INNER JOIN dbo.SalesOrderHeader SH ON SH.SalesOrderID = SD.SalesOrderID
	GROUP BY SD.ProductID
	) PO ON PO.ProductID = P.ProductID
LEFT JOIN dbo.ProductSubcategory PC ON PC.ProductSubcategoryID = P.ProductSubcategoryID
ORDER BY P.ProductName

-- Task 14
SELECT P.ProductID,
	P.ProductName,
	P.ListPrice Prod_ListPrice,
	PL.PriceHist_LatestListPrice,
	P.ListPrice - PL.PriceHist_LatestListPrice Diff
FROM dbo.Product P
INNER JOIN (
	SELECT DISTINCT ProductID,
		LAST_VALUE(ListPrice) OVER (
			PARTITION BY ProductID ORDER BY ProductID
			) AS PriceHist_LatestListPrice
	FROM dbo.ProductListPriceHistory
	) PL ON PL.ProductID = P.ProductID
WHERE P.ListPrice - PL.PriceHist_LatestListPrice = 1
	OR P.ListPrice - PL.PriceHist_LatestListPrice = - 1

-- Task 15
SELECT DISTINCT P.ProductID,
	PS.OrderDate,
	P.ProductName,
	PS.OrderQty,
	SellStartDate,
	SellEndDate
FROM dbo.Product P
INNER JOIN (
	SELECT SD.ProductID,
		SD.OrderQty,
		SH.OrderDate
	FROM dbo.SalesOrderDetail SD
	INNER JOIN dbo.SalesOrderHeader SH ON SH.SalesOrderID = SD.SalesOrderID
	) PS ON PS.ProductID = P.ProductID
WHERE SellStartDate > PS.OrderDate
	OR SellEndDate < PS.OrderDate
ORDER BY P.ProductID

-- Task 16
SELECT DISTINCT P.ProductID,
	PS.OrderDate,
	PS.OrderQty,
	SellStartDate,
	SellEndDate,
	CASE 
		WHEN SellEndDate < PS.OrderDate
			THEN 'Sold after end date'
		ELSE 'Sold before start date'
		END ProblemType
FROM dbo.Product P
INNER JOIN (
	SELECT SD.ProductID,
		SD.OrderQty,
		SH.OrderDate
	FROM dbo.SalesOrderDetail SD
	INNER JOIN dbo.SalesOrderHeader SH ON SH.SalesOrderID = SD.SalesOrderID
	) PS ON PS.ProductID = P.ProductID
WHERE SellStartDate > PS.OrderDate
	OR SellEndDate < PS.OrderDate
ORDER BY P.ProductID

-- Task 17
-- Task 18
SELECT Product.ProductID,
	ProductName,
	ProductSubCategoryName,
	FirstOrder = Convert(DATE, Min(OrderDate)),
	LastOrder = Convert(DATE, Max(OrderDate))
FROM Product
LEFT JOIN SalesOrderDetail Detail ON Product.ProductID = Detail.ProductID
LEFT JOIN SalesOrderHeader Header ON Header.SalesOrderID = Detail.SalesOrderID
LEFT JOIN ProductSubCategory ON ProductSubCategory.ProductSubCategoryID = Product.ProductSubCategoryID
WHERE Color = 'Silver'
GROUP BY Product.ProductID,
	ProductName,
	ProductSubCategoryName
ORDER BY LastOrder DESC

-- It is necessary to remove the "'here, which is written between the color' 'characters
-- Task 19
SELECT ProductID,
	ProductName,
	StandardCost,
	ListPrice,
	ListPrice - StandardCost RawMargin,
	NTILE(4) OVER (
		ORDER BY ListPrice - StandardCost DESC
		) Quartile
FROM dbo.Product
WHERE StandardCost <> 0
	AND ListPrice <> 0
	AND StandardCost / ListPrice > 0.25
ORDER BY ProductName

-- Task 20
SELECT C.CustomerID,CONCAT(C.FirstName,' ',C.LastName) CustomerName,
		COUNT(DISTINCT SH.SalesPersonEmployeeID) TotalDifferentSalesPeople
  FROM dbo.Customer C
  JOIN SalesOrderHeader SH
  ON SH.CustomerID=C.CustomerID
  GROUP BY C.CustomerID,C.FirstName,C.LastName
  HAVING COUNT(DISTINCT SH.SalesPersonEmployeeID)>1
  ORDER BY C.FirstName

-- Task 21

-- Task 22 
Select ProductName FROM dbo.Product
GROUP BY ProductName
HAVING COUNT(*)>1

-- Task 23 
SELECT (
		SELECT TOP 1 ProductID
		FROM dbo.Product
		WHERE ProductName = P.ProductName
		ORDER BY ProductID DESC
		) PotentialDuplicateProductID,
	P.ProductName
FROM dbo.Product P
GROUP BY P.ProductName
HAVING COUNT(*) > 1

-- Task 24 
Select ProductCount as TotalPriceChanges,COUNT(*) as TotalProducts FROM (Select Count(*) ProductCount FROM dbo.ProductCostHistory
GROUP BY ProductID) as tblCost
GROUP BY ProductCount

-- Task 25
Select ProductID,ProductNumber, CHARINDEX('-',ProductNumber) HyphenLocation,
CASE WHEN SUBSTRING(ProductNumber,0,CHARINDEX('-',ProductNumber))='' THEN ProductNumber ELSE SUBSTRING(ProductNumber,0,CHARINDEX('-',ProductNumber)) END BaseProductNumber,
CASE WHEN SUBSTRING(ProductNumber,0,
CHARINDEX('-',ProductNumber))='' THEN NULL ELSE SUBSTRING(ProductNumber,CHARINDEX('-',ProductNumber)+1,LEN(ProductNumber)) END Size  From dbo.Product WHERE ProductID>533

-- Task 26
With tblTmpProduct as (Select
CASE WHEN SUBSTRING(ProductNumber,0,CHARINDEX('-',ProductNumber))='' THEN ProductNumber ELSE SUBSTRING(ProductNumber,0,CHARINDEX('-',ProductNumber)) END BaseProductNumber
,ProductSubcategoryID From dbo.Product WHERE  ProductID>533)

SELECT BaseProductNumber ,
       COUNT(*) AS TotalSizes
FROM tblTmpProduct
    INNER JOIN ProductSubcategory PC ON  tblTmpProduct.ProductSubcategoryID=PC.ProductSubcategoryID
WHERE PC.ProductCategoryID=3
GROUP BY BaseProductNumber;

-- Task 27
With tblCostChange as (
Select ProductID,COUNT(*) TotalCostChanges
From dbo.ProductCostHistory
GROUP BY ProductID
)

Select C.ProductID,ProductName,C.TotalCostChanges
From dbo.Product P
INNER JOIN tblCostChange C
On C.ProductID=P.ProductID