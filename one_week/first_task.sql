CREATE DATABASE HR;

CREATE TABLE EMPLOYEE (
	_ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	Employee_ID AS RIGHT('0000' + CONVERT(VARCHAR(5), _ID), 5),
	Employee_FName VARCHAR(50) NOT NULL,
	Employee_LName VARCHAR(50) NOT NULL,
	Employee_HireDate DATE,
	Employee_Title VARCHAR(50) DEFAULT 'unknown',
	)

INSERT INTO EMPLOYEE (
	Employee_FName,
	Employee_LName,
	Employee_HireDate,
	Employee_Title
	)
VALUES (
	'Johnny',
	'Jones',
	'1995-02-14',
	'DBA'
	)

INSERT INTO EMPLOYEE (
	Employee_FName,
	Employee_LName,
	Employee_HireDate,
	Employee_Title
	)
VALUES (
	'Franklin',
	'Johnson',
	'2002-03-15',
	'Purchasing Agent'
	)

INSERT INTO EMPLOYEE (
	Employee_FName,
	Employee_LName,
	Employee_HireDate,
	Employee_Title
	)
VALUES (
	'Patricia',
	'Richards',
	'2004-06-11',
	'DBA'
	)

INSERT INTO EMPLOYEE (
	Employee_FName,
	Employee_LName,
	Employee_HireDate,
	Employee_Title
	)
VALUES (
	'Jasmine',
	'Patel',
	'2006-07-28',
	'Programmer'
	)

SELECT Employee_ID,
	Employee_FName,
	Employee_LName,
	FORMAT(getdate(), 'MM/dd/yyyy') AS Employee_HireDate,
	Employee_Title
FROM EMPLOYEE

CREATE TABLE SKILL (
	Skill_ID INT NOT NULL PRIMARY KEY IDENTITY(100, 10),
	Skill_Name VARCHAR(50) NOT NULL,
	Skill_Description TEXT NOT NULL,
	)

INSERT INTO SKILL (
	Skill_Name,
	Skill_Description
	)
VALUES (
	'BASIC DATABASE MANAGEMENT',
	'CREATE AND manage database user accounts'
	)

INSERT INTO SKILL (
	Skill_Name,
	Skill_Description
	)
VALUES (
	'BASIC Web Design',
	'CREATE AND maintain HTML and CSS documantions'
	)

INSERT INTO SKILL (
	Skill_Name,
	Skill_Description
	)
VALUES (
	'Advanced Spreadsheets',
	'CREATE AND maintain HTML and CSS documantions'
	)

SELECT *
FROM SKILL;

CREATE TABLE CERTIFIED (
	Employee_ID INT FOREIGN KEY REFERENCES EMPLOYEE(Employee_ID),
	Skill_ID INT NOT NULL REFERENCES SKILL(Skill_ID),
	Certified_Date DATE NOT NULL
	)

INSERT INTO CERTIFIED (
	Employee_ID,
	Skill_ID,
	Certified_Date
	)
VALUES (
	00001,
	100,
	'2006-07-28'
	)

INSERT INTO CERTIFIED (
	Employee_ID,
	Skill_ID,
	Certified_Date
	)
VALUES (
	00002,
	110,
	'2006-07-28'
	)

SELECT left('00000', 5 - len(Employee_ID)) + convert(VARCHAR(5), Employee_ID),
	Skill_ID,
	FORMAT(getdate(), 'MM/dd/yyyy') AS Certified_Date
FROM CERTIFIED
